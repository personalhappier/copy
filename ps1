# Function to check if the current working directory ends with the specified path
function Check-WorkingDirectory {
    param (
        [string]$ExpectedEnding
    )

    # Get the current working directory
    $currentDirectory = (Get-Location).Path

    # Check if the current working directory ends with the expected path
    if ($currentDirectory -like "*$ExpectedEnding") {
        Write-Host "The current working directory is correct: $currentDirectory"
    } else {
        Write-Host "The current working directory '$currentDirectory' does not end with '$ExpectedEnding'."
        exit 1
    }
}

# Define the expected ending of the working directory
$expectedEnding = "\dir1\dir2\my_working_dir"

# Check the working directory
Check-WorkingDirectory -ExpectedEnding $expectedEnding