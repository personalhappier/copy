package com.mycompany.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;

public class CucumberReportCleaner {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java CucumberReportCleaner <input_json_file> <output_json_file>");
            return;
        }

        String inputFile = args[0];
        String outputFile = args[1];

        try {
            removeBeforeHooksFromReport(inputFile, outputFile);
            System.out.println("Cleaned report saved to: " + outputFile);
        } catch (IOException e) {
            System.err.println("Error processing JSON file: " + e.getMessage());
        }
    }

    public static void removeBeforeHooksFromReport(String inputFile, String outputFile) throws IOException {
        File inputJson = new File(inputFile);
        if (!inputJson.exists()) {
            System.err.println("Input file not found: " + inputFile);
            return;
        }

        JsonNode rootNode = objectMapper.readTree(inputJson);

        if (rootNode.isArray()) {
            for (JsonNode featureNode : rootNode) {
                if (featureNode.has("elements") && featureNode.get("elements").isArray()) {
                    ArrayNode elements = (ArrayNode) featureNode.get("elements");

                    for (JsonNode scenarioNode : elements) {
                        if (scenarioNode.has("before")) {
                            ((ObjectNode) scenarioNode).remove("before");
                        }
                    }
                }
            }
        }

        objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(outputFile), rootNode);
    }
}




        <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>exec-maven-plugin</artifactId>
            <version>3.1.0</version>
            <executions>
                <execution>
                    <id>clean-cucumber-json-report</id>
                    <phase>verify</phase>  <!-- Runs after tests -->
                    <goals>
                        <goal>java</goal>
                    </goals>
                    <configuration>
                        <mainClass>com.mycompany.util.CucumberReportCleaner</mainClass>
                        <arguments>
                            <argument>target/cucumber-reports/cucumber.json</argument>
                            <argument>target/cucumber-reports/cucumber_cleaned.json</argument>
                        </arguments>
                    </configuration>
                </execution>
            </executions>
        </plugin>
