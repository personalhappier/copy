$containerName = "localPostgres"
$timeoutInSeconds = 60

function WaitForDockerContainer {
    param (
        [string]$containerName,
        [int]$timeoutInSeconds
    )
    
    $startTime = Get-Date
    $endTime = $startTime.AddSeconds($timeoutInSeconds)

    while ($true) {
        $containerStatus = docker inspect -f "{{.State.Running}}" $containerName 2>&1

        if ($containerStatus -eq "true") {
            Write-Output "Container '$containerName' has started."
            return $true
        }

        if ((Get-Date) -ge $endTime) {
            Write-Error "Timeout: Container '$containerName' did not start within $timeoutInSeconds seconds."
            return $false
        }

        Start-Sleep -Seconds 1
    }
}

# Start of the main script
Write-Output "Starting main script..."

# Example: Run a Docker container (modify this command as needed)
Write-Output "Starting Docker container..."
docker run -d --name $containerName your_docker_image

# Wait for the container to start
Write-Output "Waiting for the Docker container to start..."
$containerStarted = WaitForDockerContainer -containerName $containerName -timeoutInSeconds $timeoutInSeconds

if (-not $containerStarted) {
    Write-Error "Failed to start Docker container. Exiting script."
    exit 1
}

# Continue with the rest of your script
Write-Output "Docker container started successfully. Continuing with the rest of the script..."

# Example: Run some commands inside the Docker container
# docker exec $containerName some_command

# Example: Stop the Docker container at the end of the script
Write-Output "Stopping Docker container..."
docker stop $containerName

Write-Output "Main script finished."
