#!/bin/bash

# Variables
SONAR_HOST="http://localhost:9000"
SONAR_TOKEN="your-sonar-token"
PROJECT_KEY="my_project_key"

# Fetch quality gate status
QUALITY_GATE_STATUS=$(curl -s -u "$SONAR_TOKEN:" \
    "$SONAR_HOST/api/qualitygates/project_status?projectKey=$PROJECT_KEY" \
    | jq -r '.projectStatus.status')

# Check the status
if [ "$QUALITY_GATE_STATUS" == "OK" ]; then
  echo "Quality Gate passed!"
else
  echo "Quality Gate failed!"
  exit 1
fi
