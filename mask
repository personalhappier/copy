import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;

public class MaskAuthTokenFilter implements Filter {

    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext context) {
        // Check if the Authorization header exists
        if (requestSpec.getHeaders().hasHeaderWithName("Authorization")) {
            // Mask the Bearer token
            String originalAuth = requestSpec.getHeader("Authorization");
            String maskedAuth = "Bearer ******";  // Replace the token part with stars
            System.out.println("Original Authorization: " + originalAuth);
            System.out.println("Masked Authorization: " + maskedAuth);

            // Temporarily replace the Authorization header for logging purposes
            requestSpec.removeHeader("Authorization");
            requestSpec.addHeader("Authorization", maskedAuth);
        }

        // Proceed with the request
        return context.next(requestSpec, responseSpec);
    }
}
