# Function to check Java version
function Get-JavaVersion {
    $javaVersionOutput = & java -version 2>&1
    if ($javaVersionOutput -match 'version "(\d+\.\d+).*"') {
        return [version]$matches[1]
    } else {
        return $null
    }
}

# Check if Java is installed and its version
$javaVersion = Get-JavaVersion
if (-not $javaVersion) {
    Write-Error "Java is not installed. Please install Java 17 or higher and try again."
    exit 1
} elseif ($javaVersion -lt [version]"17.0") {
    Write-Error "Java version is less than 17. Please install Java 17 or higher and try again."
    exit 1
}

# Check if Docker is installed
if (-not (Get-Command "docker" -ErrorAction SilentlyContinue)) {
    Write-Error "Docker is not installed. Please install Docker and try again."
    exit 1
}

# Define the container name and image name
$containerName = "your_container_name"
$imageName = "your_image_name"

# Remove the container if it exists, otherwise continue silently
$removeContainerCmd = "docker rm -f $containerName"
Invoke-Expression $removeContainerCmd -ErrorAction SilentlyContinue

# Start the container from the image (will download image if it does not exist)
$startContainerCmd = "docker run -d --name $containerName $imageName"
Invoke-Expression $startContainerCmd

# Run Maven tests
$mvnTestCmd = "mvn test"
Invoke-Expression $mvnTestCmd
